package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
    "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	//"strings"
)

// error response contains everything we need to use http.Error
type handlerError struct {
	Error   error
	Message string
	Code    int
}

// book model

type Logs struct{
	
    Push_Code string `bson:"push_code" json:"push_code"`
    App_ID string `bson:"app_id" bson:"app_id"`
    User_ID string `bson:"user_id" bson:"user_id"`
    Device_ID string `bson:"device_id" bson:"device_id"`
    Date_Access string `bson:"date_access" bson:"date_access"`
    Message string `bson:"message" bson:"message"`
    IP_Address string `bson:"ip_address" bson:"ip_address"`
    Status string `bson:"status" bson:"status"`
    Id     int    `json:"id"`
}

// list of all of the books
var logs = make([]Logs, 0)

// a custom type that we can use for handling errors and formatting responses
type handler func(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)

// attach the standard ServeHTTP method to our handler so the http library can call it
func (fn handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// here we could do some prep work before calling the handler if we wanted to

	// call the actual handler
	response, err := fn(w, r)

	// check for errors
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}
	if response == nil {
		log.Printf("ERROR: response from method is nil\n")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	// turn the response into JSON
	bytes, e := json.Marshal(response)
	if e != nil {
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	fmt.Printf(string(bytes))

	// send the response and log
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)
}

func listLogs(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	return logs, nil
}

func getLog(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	// mux.Vars grabs variables from the path
	param := mux.Vars(r)["id"]
	id, e := strconv.Atoi(param)
	if e != nil {
		return nil, &handlerError{e, "Id should be an integer", http.StatusBadRequest}
	}
	b, index := getLogById(id)

	if index < 0 {
		return nil, &handlerError{nil, "Could not find book " + param, http.StatusNotFound}
	}

	return b, nil
}

func parseLogRequest(r *http.Request) (Logs, *handlerError) {
	// the book payload is in the request body
	data, e := ioutil.ReadAll(r.Body)
	if e != nil {
		return Logs{}, &handlerError{e, "Could not read request", http.StatusBadRequest}
	}

	// turn the request body (JSON) into a book object
	var payload Logs
	e = json.Unmarshal(data, &payload)
	if e != nil {
		return Logs{}, &handlerError{e, "Could not parse JSON", http.StatusBadRequest}
	}

	return payload, nil
}

func addLog(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	payload, e := parseLogRequest(r)
	if e != nil {
		return nil, e
	}

	// it's our job to assign IDs, ignore what (if anything) the client sent
	payload.Id = getNextId()
	logs = append(logs, payload)

	// we return the book we just made so the client can see the ID if they want
	return payload, nil
}

func updateLog(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	payload, e := parseLogRequest(r)
	if e != nil {
		return nil, e
	}

	_, index := getLogById(payload.Id)
	logs[index] = payload
	return make(map[string]string), nil
}

func removeLog(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	param := mux.Vars(r)["id"]
	id, e := strconv.Atoi(param)
	if e != nil {
		return nil, &handlerError{e, "Id should be an integer", http.StatusBadRequest}
	}
	// this is jsut to check to see if the book exists
	_, index := getLogById(id)

	if index < 0 {
		return nil, &handlerError{nil, "Could not find entry " + param, http.StatusNotFound}
	}

	// remove a book from the list
	logs = append(logs[:index], logs[index+1:]...)
	return make(map[string]string), nil
}


// searches the books for the book with `id` and returns the book and it's index, or -1 for 404

func getLogById(id int) (Logs, int) {
	for i, b := range logs {
		if b.Id == id {
			return b, i
		}
	}
	return Logs{}, -1
}


var id = 0

// increments id and returns the value
func getNextId() int {
	id += 1
	return id
}

func main() {
	// command line flags
	port := flag.Int("port", 80, "port to serve on")
	dir := flag.String("directory", "web/", "directory of web files")
	flag.Parse()

	// handle all requests by serving a file of the same name
	fs := http.Dir(*dir)
	fileHandler := http.FileServer(fs)

	// setup routes
	router := mux.NewRouter()
	router.Handle("/", http.RedirectHandler("/static/", 302))
	router.Handle("/logs", handler(listLogs)).Methods("GET")
	router.Handle("/logs", handler(addLog)).Methods("POST")
	router.Handle("/logs/{id}", handler(getLog)).Methods("GET")
	router.Handle("/logs/{id}", handler(updateLog)).Methods("POST")
	router.Handle("/logs/{id}", handler(removeLog)).Methods("DELETE")

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static", fileHandler))
	http.Handle("/", router)



//connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("logs")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Logs{}
        iter := c.Find(bson.M{}).Iter()
       
       	var loop int;
       	loop=0
        for iter.Next(&result){
                fmt.Println("RESULT #",loop)
                fmt.Println("Push Code   : ", result.Push_Code)
                fmt.Println("App ID      : ", result.App_ID)
                fmt.Println("User ID     : ", result.User_ID)
                fmt.Println("Device ID   : ", result.Device_ID)
                fmt.Println("Date Access : ", result.Date_Access)
                fmt.Println("Message     : ", result.Message)
                fmt.Println("IP Address  : ", result.IP_Address)
                fmt.Println("Status      : ", result.Status)
                fmt.Println()

                logs = append(logs, Logs{result.Push_Code, result.App_ID,result.User_ID,result.Device_ID, result.Date_Access, result.Message, result.IP_Address, result.Status,getNextId()})
                loop++
        }	

	log.Printf("Running on port %d\n", *port)

	addr := fmt.Sprintf("127.0.0.1:%d", *port)
	// this call blocks -- the progam runs here forever
	err = http.ListenAndServe(addr, nil)
	fmt.Println(err.Error())
}